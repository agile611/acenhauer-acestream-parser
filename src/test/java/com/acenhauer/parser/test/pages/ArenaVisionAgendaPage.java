package com.acenhauer.parser.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem.hernandez on 18/12/2014.
 */
public class ArenaVisionAgendaPage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    public ArenaVisionAgendaPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 2);
    }

    public String getAgenda(WebDriver driver) {
        return driver.findElement(By.xpath(".//*[@id='main']/div/div/div/div/p[2]")).getText();
    }
}
