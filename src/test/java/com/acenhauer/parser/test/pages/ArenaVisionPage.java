package com.acenhauer.parser.test.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem.hernandez on 18/12/2014.
 */
public class ArenaVisionPage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    public ArenaVisionPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 2);
    }

    public String getAceStreamLink(WebDriver driver) {
        return driver.findElement(By.xpath(
                ".//*[@id='main']/div/div/div/div/div[3]/table/tbody/tr/td/div/div[2]/p[2]/a[2]"))
                .getAttribute("href").toString();
    }

    public String getAceStreamLinkFromResponse(String response) {
        String[] acestreamParsing = response.split("acestream://");
        String[] aceStreamLink = acestreamParsing[1].split("\"");
        System.out.println("acestream://" + aceStreamLink[0]);
        return "acestream://" + aceStreamLink[0];
    }
}
