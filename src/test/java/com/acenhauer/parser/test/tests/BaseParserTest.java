package com.acenhauer.parser.test.tests;

import com.acenhauer.parser.helpers.ChannelInfo;
import com.acenhauer.parser.helpers.Events;
import com.acenhauer.parser.helpers.SportEventz;
import com.acenhauer.parser.selenium.BaseAcenhauerWebDriver;
import org.apache.commons.lang3.StringEscapeUtils;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by guillem on 18/07/15.
 */
public class BaseParserTest extends BaseAcenhauerWebDriver {

    @Test
    public void parser() throws Exception {

        HashMap<String, List<String>> agendaChannels = new HashMap<>();
        ArrayList<ChannelInfo> channels = new ArrayList<>();
        ArrayList<Events> listOfEvents = new ArrayList<>();
        SportEventz sportEventzParsing = new SportEventz();

        TimeZone tz = TimeZone.getTimeZone("Europe/Paris");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd HH:mm z");
        dateFormat.setTimeZone(tz);
        Calendar today = Calendar.getInstance();
        Date now = new Date(today.getTimeInMillis());

        channels = sportEventzParsing.parseFromAceTV("sport", channels);

        channels = sportEventzParsing.parseFromTorrentTV(channels);

        channels = sportEventzParsing.parseFromLiveFootbalol(driver(), channels);

        channels = sportEventzParsing.parseFromArenaVision(driver(), channels);

        listOfEvents = sportEventzParsing.addingAgendaFromArenaVision(driver(), now, agendaChannels, channels, listOfEvents, dateFormat);

        listOfEvents = sportEventzParsing.addingEventsFromSportEvents(driver(), now, listOfEvents, channels);

        listOfEvents = sportEventzParsing.addingMotorSports(listOfEvents, channels);

        channelFileCreation(listOfEvents, sportEventzParsing, channels);
    }

    private void channelFileCreation(ArrayList<Events> listOfEvents, SportEventz sportEventzParsing, ArrayList<ChannelInfo> channels) throws FileNotFoundException {
        //Generation of events from the available objects
        String textChannels = "{";
        for (Events event : listOfEvents) {
            if (!event.channelsInfo.isEmpty()) {
                textChannels += "'" + event.eventName.replaceAll("'", "") + "': [";
                event.channelsInfo = sportEventzParsing.removeTheDuplicates(event.channelsInfo);
                for (ChannelInfo channelInfo : event.channelsInfo) {
                    textChannels +=
                            "{'name': '" + channelInfo.channelName.replaceAll("'", "") + "',"
                                    + "'thumb': '" + channelInfo.thumb + "'," + "'video': [";
                    ChannelInfo chd =
                            sportEventzParsing.getChannelFromList(channels, channelInfo.channelName);
                    for (int r = 0; r < chd.aceLinks.size(); r++) {
                        textChannels +=
                                "{'linkName' : '" + chd.aceLinks.get(r).source + "','aceLink': '"
                                        + chd.aceLinks.get(r).acelink + "'},";
                    }
                    textChannels = textChannels.substring(0, textChannels.length() - 1);
                    textChannels += "]," + "'genre': '" + channelInfo.genre + "'},";
                }
                textChannels = textChannels.substring(0, textChannels.length() - 1);
                textChannels += "],";
            }
        }

        textChannels += "'Sports Channels': [";
        for (ChannelInfo channel : channels) {
            textChannels +=
                    "{'name': '" + channel.channelName + "'," + "'thumb': '" + sportEventzParsing
                            .getLogo(channel.channelName) + "'," + "'video': [";
            for (int r = 0; r < channel.aceLinks.size(); r++) {
                textChannels +=
                        "{'linkName' : '" + channel.aceLinks.get(r).source + "','aceLink': '"
                                + channel.aceLinks.get(r).acelink + "'},";
            }
            textChannels = textChannels.substring(0, textChannels.length() - 1);
            textChannels += "]," + "'genre': '" + channel.genre + "'},";
        }

        textChannels = textChannels.substring(0, textChannels.length() - 1);
        textChannels += "]}";

        textChannels =
                StringEscapeUtils.unescapeHtml4(textChannels).replaceAll("[^\\x20-\\x7e]", "");

        textChannels = textChannels.replaceAll("\\+", "");
        textChannels = textChannels.replaceAll("ESPAA/", "");
        textChannels = textChannels.replaceAll("FRANCIA/", "");
        textChannels = textChannels.replaceAll("ALEMANIA/", "");
        textChannels = textChannels.replaceAll("EEUU", "USA");
        textChannels = textChannels.replaceAll("INGLATERRA", "ENGLAND");
        textChannels = textChannels.replaceAll("(SOCCER) AMERICANO", "(AMERICAN FOOTBALL)");
        textChannels = textChannels.replaceAll("FUTBOL", "(SOCCER)");
        textChannels = textChannels.replaceAll("BOXEO", "(BOXING)");
        textChannels = textChannels.replaceAll("BALONCESTO", "(BASKETBALL)");
        textChannels = textChannels.replaceAll("TENIS", "(TENNIS)");
        textChannels = textChannels.replaceAll("RUGBY", "(RUGBY)");
        textChannels = textChannels.replaceAll("AMISTOSO", "FRIENDLY");
        textChannels = textChannels.replaceAll("MOTOCICLISMO", "(MOTO GP)");
        textChannels = textChannels.replaceAll("FORMULA 1", "(F1)");
        textChannels = textChannels.replaceAll("CARRERA", "RACE");
        textChannels = textChannels.replaceAll("BEISBOL", "(BASEBALL)");
        textChannels = textChannels.replaceAll("24:", "00:");
        textChannels = textChannels.replaceAll("ARTES MARCIALES MIXTAS", "(MIXED MARTIAL ARTS)");
        textChannels = textChannels.replaceAll("}\\{", "},{");
        textChannels = textChannels.replaceAll("]]", "]");
        textChannels = textChannels.replaceAll("\\\\n", "");
        //System.out.println(textChannels);
        PrintWriter out = new PrintWriter("acenhauer-channels.txt");
        out.println(textChannels);
        out.close();
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch (NumberFormatException e) {
            return false;
        } catch (NullPointerException e) {
            return false;
        }
        // only got here if we didn't return false
        return true;
    }
}
