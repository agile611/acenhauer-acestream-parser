package com.acenhauer.parser.helpers;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by guillem on 15/01/16.
 */

public class ChannelInfo {

    public String channelName;
    public String thumb;
    public String genre;
    public List<AceLink> aceLinks;

    public ChannelInfo(String channelName, String thumb, String genre) {
        this.channelName = channelName;
        this.thumb = thumb;
        this.genre = genre;
        this.aceLinks = new ArrayList<>();
    }
}
