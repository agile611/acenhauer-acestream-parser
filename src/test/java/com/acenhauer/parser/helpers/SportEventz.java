package com.acenhauer.parser.helpers;

import com.acenhauer.parser.test.pages.ArenaVisionPage;
import org.apache.commons.lang3.time.DateUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.openqa.selenium.*;
import us.codecraft.xsoup.Xsoup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.acenhauer.parser.test.tests.BaseParserTest.isInteger;

/**
 * Created by guillem on 18/01/16.
 */
public class SportEventz {

    public SportEventz() {
    }

    public ArrayList<Events> sportEventzParsing(String SportEventzHtml,
                                                ArrayList<Events> listOfEvents, ArrayList<ChannelInfo> channels, String sportGenre) {
        try {
            Document SportEventzFootballDoc = Jsoup.parse(SportEventzHtml);

            Elements numberOfEvents =
                    Xsoup.compile("//div[@id='MagicTableContainer']/div/table/tbody/tr")
                            .evaluate(SportEventzFootballDoc).getElements();

            int events = numberOfEvents.size();

            System.out.println("Number of events: " + events);
            for (int i = 1; i < events + 1; i++) {
                String eventNameXpath =
                        "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i + "]/td/div/div[2]/h1";
                String eventName = getTextFromXSoupSelect(SportEventzFootballDoc, eventNameXpath);
                String eventTimeXpath =
                        "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i + "]/td/div/div[3]/h3";
                String eventTime = getTextFromXSoupSelect(SportEventzFootballDoc, eventTimeXpath);
                String numberOfChannelsXpath =
                        "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                                + "]/td/div/div[@class='MagicTableChannels']/button";
                Elements numberOfChannelsFromEvent =
                        Xsoup.compile(numberOfChannelsXpath).evaluate(SportEventzFootballDoc).getElements();

                int numberOfChannels = numberOfChannelsFromEvent.size();

                if (!eventName.isEmpty()) {
                    if (checkIfEventIsOnTheList(listOfEvents, eventName, eventTime)) {
                        System.out.println("Checking potential new channels for an EXISTING event " + eventName);
                        for (int k = 0; k < listOfEvents.size(); k++) {
                            String listOfEventsName = listOfEvents.get(k).eventName;
                            if (listOfEventsName.contains("-")) {
                                String[] elements = listOfEventsName.split("-");
                                String[] local = elements[0].split(":");
                                String[] visitor = elements[1].split("\\(");
                                String localTeam = local[2].replace(" ", "").toLowerCase();
                                String visitorTeam = visitor[0].replace(" ", "").toLowerCase();
                                String[] eventFromURL = eventName.split("vs.");
                                String localTeamFromURLToLowerCase =
                                        eventFromURL[0].replace(" ", "").toLowerCase();
                                String visitorTeamFromURLToLowerCase =
                                        eventFromURL[1].replace(" ", "").toLowerCase();
                                if (localTeamFromURLToLowerCase.contains(localTeam)
                                        || visitorTeamFromURLToLowerCase.contains(visitorTeam)) {
                                    AddingNewChannelsToAnExistingEvent(i, k, numberOfChannels, SportEventzFootballDoc, listOfEvents, channels);
                                }
                            }
                        }
                    } else {
                        System.out.println("Checking potential new channels for an NEW event " + eventName);
                        ArrayList<ChannelInfo> channelsInfo = new ArrayList<>();
                        String[] newTime = eventTime.split(" ");
                        String year = newTime[3].substring(2);
                        String month = getMonthNumber(newTime[2]);
                        String formerTime[] = newTime[4].split(":");
                        int hourInt = Integer.parseInt(formerTime[0]) + 1;
                        String hour;
                        if (hourInt < 10) {
                            hour = Integer.toString(hourInt);
                            hour = "0" + hour;
                        } else {
                            hour = Integer.toString(hourInt);
                        }
                        String day = newTime[1];
                        String newEventName =
                                year + "/" + month + "/" + day + " " + hour + ":" + formerTime[1] + " CET ("
                                        + sportGenre + "): " + eventName;
                        newEventName = newEventName.replaceAll("vs.", "-").toUpperCase();
                        System.out.println("Adding new event " + eventName);
                        for (int j = 1; j <= numberOfChannels; j++) {
                            String originalBroadcasterNameXpath =
                                    "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                                            + "]/td/div/div[@class='MagicTableChannels']/button[" + j + "]/div";
                            String originalBroadcasterName =
                                    getTextFromXSoupSelect(SportEventzFootballDoc,
                                            originalBroadcasterNameXpath);
                            for (int w = 0; w < channels.size(); w++) {
                                String broadcaster =
                                        originalBroadcasterName.replace(" ", "").toLowerCase();
                                String formerChannel =
                                        channels.get(w).channelName.replace(" ", "").toLowerCase();
                                if (formerChannel.contains(broadcaster)) {
                                    System.out.println(
                                            "New Event AceStream Link from " + originalBroadcasterName
                                                    + "!!");
                                    if (!checkIfChannelIsInTheList(channelsInfo,
                                            channels.get(w).channelName)) {
                                        channelsInfo.add(new ChannelInfo(channels.get(w).channelName,
                                                getLogo(channels.get(w).channelName),
                                                getCategory(channels.get(w).channelName)));
                                    }
                                }
                            }
                        }
                        if (!channelsInfo.isEmpty()) {
                            listOfEvents.add(new Events(newEventName.toUpperCase(), channelsInfo));
                        }
                    }
                }
            }
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            //Do Nothing
        }
        return listOfEvents;
    }

    public void AddingNewChannelsToAnExistingEvent(int i, int k, int numberOfChannels, Document SportEventzFootballDoc,
                                                   ArrayList<Events> listOfEvents, ArrayList<ChannelInfo> channels) {
        for (int j = 1; j <= numberOfChannels; j++) {
            String originalBroadcasterNameXpath =
                    "//div[@id='MagicTableContainer']/div/table/tbody/tr[" + i
                            + "]/td/div/div[@class='MagicTableChannels']/button["
                            + j + "]/div";
            String originalBroadcasterName =
                    getTextFromXSoupSelect(SportEventzFootballDoc,
                            originalBroadcasterNameXpath);
            for (int w = 0; w < channels.size(); w++) {
                String broadcaster =
                        originalBroadcasterName.replace(" ", "").toLowerCase();
                String formerChannel =
                        channels.get(w).channelName.replace(" ", "").toLowerCase();
                if (broadcaster.contains(formerChannel)) {
                    if (!checkIfChannelIsInTheList(
                            listOfEvents.get(k).channelsInfo,
                            channels.get(w).channelName)) {
                        listOfEvents.get(k).channelsInfo.add(
                                new ChannelInfo(channels.get(w).channelName,
                                        getLogo(channels.get(w).channelName),
                                        getCategory(channels.get(w).channelName)));
                    }
                    break;
                }
            }
        }
    }

    private boolean checkIfEventIsOnTheList(ArrayList<Events> listOfEvents, String eventName,
                                            String eventTime) {
        for (int k = 0; k < listOfEvents.size(); k++) {
            String listOfEventsName = listOfEvents.get(k).eventName;
            if (listOfEventsName.contains(" - ")) {
                listOfEventsName = listOfEventsName.toLowerCase();
                String[] elements = listOfEvents.get(k).eventName.split("-");
                String[] local = elements[0].split(":");
                String[] visitor = elements[1].split("\\(");
                String localTeam = local[2].replace(" ", "").toLowerCase();
                String visitorTeam = visitor[0].replace(" ", "").toLowerCase();
                String[] eventFromURL = eventName.split("vs.");
                String localTeamFromURLToLowerCase = eventFromURL[0].replace(" ", "").toLowerCase();
                String visitorTeamFromURLToLowerCase =
                        eventFromURL[1].replace(" ", "").toLowerCase();
                String[] newTime = eventTime.split(" ");
                String year = newTime[3].substring(2);
                String month = getMonthNumber(newTime[2]);
                String day = newTime[1];
                if ((listOfEventsName.contains(month) && listOfEventsName.contains(day)
                        && listOfEventsName.contains(year)) && ((localTeamFromURLToLowerCase.contains(localTeam) || visitorTeamFromURLToLowerCase
                        .contains(visitorTeam)))) {
                    return true;
                }
            }

        }
        return false;
    }

    public boolean checkIfLiveSoccerEventIsOnTheList(ArrayList<Events> listOfEvents, String local,
                                                     String visitor) {
        for (int k = 0; k < listOfEvents.size(); k++) {
            String listOfEventsName = listOfEvents.get(k).eventName.toLowerCase();
            if (listOfEventsName.contains(local) || listOfEventsName.contains(visitor))
                return true;
        }
        return false;
    }

    public Events getEventFromLocalAndVisitor(ArrayList<Events> listOfEvents, String local,
                                              String visitor) {
        for (int k = 0; k < listOfEvents.size(); k++) {
            String listOfEventsName = listOfEvents.get(k).eventName.toLowerCase();
            if (listOfEventsName.contains(local) || listOfEventsName.contains(visitor))
                return listOfEvents.get(k);
        }
        return null;
    }

    public String getMonthNumber(String NombreMes) {

        String NumeroMes;

        switch (NombreMes.toLowerCase()) {

            case ("january"):
                NumeroMes = "01";
                return NumeroMes;

            case ("february"):
                NumeroMes = "02";
                return NumeroMes;

            case ("march"):
                NumeroMes = "03";
                return NumeroMes;

            case ("april"):
                NumeroMes = "04";
                return NumeroMes;

            case ("may"):
                NumeroMes = "05";
                return NumeroMes;

            case ("june"):
                NumeroMes = "06";
                return NumeroMes;

            case ("july"):
                NumeroMes = "07";
                return NumeroMes;

            case ("august"):
                NumeroMes = "08";
                return NumeroMes;

            case ("september"):
                NumeroMes = "09";
                return NumeroMes;

            case ("october"):
                NumeroMes = "10";
                return NumeroMes;

            case ("november"):
                NumeroMes = "11";
                return NumeroMes;

            case ("december"):
                NumeroMes = "12";
                return NumeroMes;

            default:
                return "ERROR";
        }
    }

    public String getTextFromXSoupSelect(Document doc, String html) {
        try {
            return Jsoup.parse(Xsoup.select(doc, html).get()).body().text();
        } catch (IllegalArgumentException e) {
            return "";
        }
    }

    public String getCategory(String key) {
        if (key.contains("Arena Vision")) {
            return "Arena Vision";
        } else if (key.contains("Acefootball")) {
            return "Acefootball";
        } else if (key.contains("Al Jazeera Sport")) {
            return "Al Jazeera Sport";
        } else if (key.contains("Arena Sport")) {
            return "Arena Sport";
        } else if (key.contains("Arenavision")) {
            return "Arenavision";
        } else if (key.contains("AzTV")) {
            return "AzTV";
        } else if (key.contains("BBC")) {
            return "BBC";
        } else if (key.contains("BeIN Sport")) {
            return "BeIN Sport";
        } else if (key.contains("Benfica TV")) {
            return "Benfica TV";
        } else if (key.contains("BT Sport")) {
            return "BT Sport";
        } else if (key.contains("C+") || key.contains("Canal+")) {
            return "Canal+";
        } else if (key.contains("Digi Sport")) {
            return "Digi Sport";
        } else if (key.contains("Eurosport")) {
            return "Eurosport";
        } else if (key.contains("ESPN")) {
            return "ESPN";
        } else if (key.contains("Fox Sports")) {
            return "Fox Sports";
        } else if (key.contains("NTV Plus")) {
            return "NTV Plus";
        } else if (key.contains("Sky Sports")) {
            return "Sky Sports";
        } else if (key.contains("TSN")) {
            return "TSN";
        } else if (key.contains("Viasat")) {
            return "Viasat";
        }
        return "Sports";
    }

    public String getLogo(String key) {
        if (key.contains("Arena Vision")) {
            return "http://kodi.altervista.org/wp-content/uploads/2015/07/arenavision.jpg";
        } else if (key.contains("Acefootball")) {
            return "http://3.bp.blogspot.com/-P2KSN2wA4h4/ViTzLGAJMgI/AAAAAAAABUM/6gY-pnaJTHk/s1600/torrent_stream_logo-300x262.png";
        } else if (key.contains("Al Jazeera Sport")) {
            return "http://www.ligue1.com/images/photos/articles/web/fiche/1011_Al_Jazeera_Sport.jpg";
        } else if (key.contains("Arena Sport")) {
            return "http://www.tvarenasport.com/themes/3way/img/logo.png";
        } else if (key.contains("AzTV")) {
            return "http://musavat.com/data/largenews/dir282/img1779691.jpg";
        } else if (key.contains("BBC")) {
            return "https://pbs.twimg.com/profile_images/2162206162/bbsport_twitter_400x400.png";
        } else if (key.contains("BeIN Sport")) {
            return "http://assets.beinsports.com/2.5.11/images/logo-header__us.png";
        } else if (key.contains("Benfica TV")) {
            return "http://tv.moviesntvlive.com/wp-content/uploads/2015/12/Benfica-TV.png";
        } else if (key.contains("BT Sport")) {
            return "https://upload.wikimedia.org/wikipedia/en/thumb/9/98/BT_Sport.svg/1280px-BT_Sport.svg.png";
        } else if (key.contains("C+") || key.contains("Canal+")) {
            return "https://pmcdeadline2.files.wordpress.com/2013/10/logo-canal-plus__131028205851.gif";
        } else if (key.contains("Digi Sport")) {
            return "http://wikitv.eu/wp-content/uploads/2015/03/DigiSport-1.png";
        } else if (key.contains("Eurosport")) {
            return "http://layout.eurosport.com/i/v8/logo/logo-esp-og-new.jpg";
        } else if (key.contains("ESPN")) {
            return "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/ESPN_wordmark.svg/2000px-ESPN_wordmark.svg.png";
        } else if (key.contains("Fox Sports")) {
            return "https://upload.wikimedia.org/wikipedia/en/archive/d/dd/20160220233658!Fox_Sports_Logo.png";
        } else if (key.contains("NTV Plus")) {
            return "https://pbs.twimg.com/profile_images/469024578312757248/ABkJJwEI.png";
        } else if (key.contains("Sky Sports")) {
            return "https://pbs.twimg.com/profile_images/504278813644054528/lSkPHorY.png";
        } else if (key.contains("TSN")) {
            return "https://upload.wikimedia.org/wikipedia/commons/thumb/9/90/TSN_Logo.svg/2000px-TSN_Logo.svg.png";
        } else if (key.contains("Viasat")) {
            return "http://vignette2.wikia.nocookie.net/logopedia/images/9/9e/Viasat_logo_2010.png";
        } else if (key.contains("Movistar")) {
            return "http://www.brandsoftheworld.com/sites/default/files/styles/logo-thumbnail/public/042011/movistar_eps.png";
        } else if (key.contains("Setanta")) {
            return "https://secure.setantasubs.com/images/setanta-logo.png";
        } else if (key.contains("Setanta")) {
            return "https://secure.setantasubs.com/images/setanta-logo.png";
        }
        return "http://3.bp.blogspot.com/-P2KSN2wA4h4/ViTzLGAJMgI/AAAAAAAABUM/6gY-pnaJTHk/s1600/torrent_stream_logo-300x262.png";
    }

    public boolean checkIfChannelIsInTheList(ArrayList<ChannelInfo> channels, String channel) {
        for (ChannelInfo entry : channels) {
            String channelFromList = prepareChannelToCompare(entry.channelName);
            String channelToCompare = prepareChannelToCompare(channel);
            if (channelFromList.contains(channelToCompare)) {
                System.out
                        .println("Channel from list: " + channelFromList + " vs " + channelToCompare);
                return true;
            }
        }
        return false;
    }

    public ArrayList<ChannelInfo> parseFromAceTV(String category, ArrayList<ChannelInfo> channels) {
        List<String> aceStreamLinks = new ArrayList<>();
        List<String> channelsAux = new ArrayList<>();
        String url = "http://torrent-tv-online.pp.ua/_tv/" + category + "/counter.js";
        String response = getStringFromUrl(url);
        String content[] = response.toString().split("\\[");
        String[] channelsFromParse = content[1].split("\\]");
        String[] channelsParsed = channelsFromParse[0].split("\"");
        for (int i = 1; i < channelsParsed.length; i++) {
            String channelName = "";
            if (i % 2 == 0) {
                channelName = channelsParsed[i].substring(5);
                String[] removedChannels = channelName.split("\\(");
                channelName = removedChannels[0];
                channelsAux.add(channelName);
            }
        }
        for (int i = 1; i < channelsParsed.length; i++) {
            String aceStreamLink = "";
            if (i % 2 != 0) {
                aceStreamLink = channelsParsed[i];
                aceStreamLinks.add(aceStreamLink);
            }
        }

        for (int w = 1; w < aceStreamLinks.size(); w++) {
            if (isUTF8MisInterpreted(channelsAux.get(w))) {
                System.out.println("Compatible channel name: " + channelsAux.get(w));
                System.out.println("Channels AceStream: " + aceStreamLinks.get(w));
                String channelNameAceTv =
                        channelsAux.get(w).replace(" AceStream", "");
                ChannelInfo channelToAdd =
                        new ChannelInfo(channelNameAceTv, getLogo(channelNameAceTv),
                                getCategory(channelNameAceTv));
                channelToAdd.aceLinks.add(new AceLink("Ace Tv", aceStreamLinks.get(w)));
                channels.add(channelToAdd);
            }
        }

        return channels;
    }

    public static boolean isUTF8MisInterpreted(String input) {
        //convenience overload for the most common UTF-8 misinterpretation
        //which is also the case in your question
        return isUTF8MisInterpreted(input, "Windows-1252");
    }

    public static boolean isUTF8MisInterpreted(String input, String encoding) {

        CharsetDecoder decoder = Charset.forName("UTF-8").newDecoder();
        CharsetEncoder encoder = Charset.forName(encoding).newEncoder();
        ByteBuffer tmp;
        try {
            tmp = encoder.encode(CharBuffer.wrap(input));
        } catch (CharacterCodingException e) {
            return false;
        }

        try {
            decoder.decode(tmp);
            return true;
        } catch (CharacterCodingException e) {
            return false;
        }
    }

    public String getStringFromUrl(String url) {
        StringBuilder response = null;
        System.out.println("Requesting parsing from ..." + url);
        URL website = null;
        try {
            website = new URL(url);
            URLConnection connection = website.openConnection();
            BufferedReader in =
                    new BufferedReader(new InputStreamReader(connection.getInputStream()));
            response = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
            in.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }

    public String prepareChannelToCompare(String originalChannel) {
        return originalChannel.toLowerCase().replace(" ", "").replace(" AceStream", "")
                .replace(" HD", "").replace(" hd", "");
    }

    public ChannelInfo getChannelFromList(ArrayList<ChannelInfo> channels, String channelName) {
        for (ChannelInfo entry : channels) {
            String channelFromList = prepareChannelToCompare(entry.channelName);
            String channelToCompare = prepareChannelToCompare(channelName);
            if (channelFromList.contains(channelToCompare)) {
                return entry;
            }
        }
        return null;
    }

    public boolean checkIfChannelIsInTheChannelInitialList(ArrayList<ChannelInfo> channels, String channelName) {
        for (ChannelInfo chan : channels) {
            String channelFromList = prepareChannelToCompare(chan.channelName);
            String channelToCompare = prepareChannelToCompare(channelName);
            if (channelFromList.contains(channelToCompare)) {
                return true;
            }
        }
        return false;
    }

    public void addingAceLinkToExistingChannel(ArrayList<ChannelInfo> channels, String channelName,
                                               String source, String aceStreamChannelLink) {
        SportEventz sep = new SportEventz();
        ChannelInfo chd = sep.getChannelFromList(channels, channelName);
        chd.aceLinks.add(new AceLink(source, aceStreamChannelLink));
    }

    public void addingNewChannel(String channelName, String source, String aceStreamChannelLink,
                                 ArrayList<ChannelInfo> channels) {
        SportEventz sep = new SportEventz();
        System.out.println("Adding new channel " + channelName);
        ChannelInfo channelToAdd =
                new ChannelInfo(channelName, sep.getLogo(channelName), sep.getCategory(channelName));
        channelToAdd.aceLinks.add(new AceLink(source, aceStreamChannelLink));
        channels.add(channelToAdd);
    }


    public ArrayList<ChannelInfo> removeTheDuplicates(ArrayList<ChannelInfo> channels) {
        // add elements to al, including duplicates
        Set<ChannelInfo> hs = new HashSet<>();
        hs.addAll(channels);
        channels.clear();
        channels.addAll(hs);
        return channels;
    }

    public ArrayList<Events> getLiveSoccerChannels(WebDriver driver, ArrayList<Events> listOfEvents, ArrayList<ChannelInfo> channels) {
        //LiveSoccerTV links
        driver.get("http://www.livesoccertv.com/");
        String pageSourceLiveSoccer = driver.getPageSource();
        Document pageSourceLiveSoccerDoc = Jsoup.parse(pageSourceLiveSoccer);
        String numberOfEventsFromLiveSoccerTV = "//table[@class='schedules']/tbody/tr/td[3]/a";
        Elements eventsFromLiveSoccer =
                Xsoup.compile(numberOfEventsFromLiveSoccerTV).evaluate(pageSourceLiveSoccerDoc).getElements();
        for (int i = 0; i < eventsFromLiveSoccer.size(); i++) {
            String urlFromEvents = eventsFromLiveSoccer.get(i).attr("href");
            if (urlFromEvents.contains("vs")) {
                driver.get("http://www.livesoccertv.com/" + urlFromEvents);
                System.out.println("http://www.livesoccertv.com/" + urlFromEvents);
                urlFromEvents = urlFromEvents.replaceAll("-", " ");
                String[] parts = urlFromEvents.split("vs");
                String[] parts2 = parts[0].split("/");
                String local = parts2[parts2.length - 1].replaceAll(" ", "");
                String visitor = parts[1].replaceAll("/", "").replaceAll(" ", "");
                if (checkIfLiveSoccerEventIsOnTheList(listOfEvents, local, visitor)) {
                    Events event = getEventFromLocalAndVisitor(listOfEvents, local, visitor);
                    Document eventSourceDoc = Jsoup.parse(driver.getPageSource());
                    String channelsFromEventLiveSoccerTv = "//table[@id='wc_channels']/tbody/tr/td[2]/a";
                    Elements channelsFromEventLSTV =
                            Xsoup.compile(channelsFromEventLiveSoccerTv).evaluate(eventSourceDoc).getElements();
                    for (int j = 0; j < channelsFromEventLSTV.size(); j++) {
                        if (checkIfChannelIsInTheChannelInitialList(channels,
                                channelsFromEventLSTV.get(j).text())) {
                            if (!checkIfChannelIsInTheList(event.channelsInfo,
                                    channelsFromEventLSTV.get(j).text())) {
                                event.channelsInfo.add(new ChannelInfo(channelsFromEventLSTV.get(j).text(),
                                        getLogo(channelsFromEventLSTV.get(j).text()),
                                        getCategory(channelsFromEventLSTV.get(j).text())));
                            }
                        }
                    }
                }
            }
        }
        return listOfEvents;
    }

    private static String getUrlSource(String sURL) throws IOException {
        System.out.println(sURL);
        URL url = new URL(sURL);
        HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
        httpCon.setConnectTimeout(5000);
        httpCon.setReadTimeout(5000);
        httpCon.setFollowRedirects(false);
        httpCon.setInstanceFollowRedirects(false);
        httpCon.setDoOutput(true);
        httpCon.setUseCaches(true);
        httpCon.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(new InputStreamReader(httpCon.getInputStream()));
        String inputLine;
        StringBuilder a = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
            a.append(inputLine);
        in.close();
        //System.out.println(a.toString());
        httpCon.disconnect();
        return a.toString();
    }

    public ArrayList<ChannelInfo> parseFromTorrentTV(ArrayList<ChannelInfo> channels) throws IOException {
        try {
            String pageSourceTorrentTV = getUrlSource("http://torrent-tv.ru/category.php?cat=4");
            Document pageSourceTorrentTVDoc = Jsoup.parse(pageSourceTorrentTV);
            String numberOfEventsFromTorrentTV = "html/body/div[2]/div[3]/div[2]/div[3]/div";
            Elements numberOfTotalChannels = Xsoup.compile(numberOfEventsFromTorrentTV).evaluate(pageSourceTorrentTVDoc).getElements();
            System.out.println(numberOfTotalChannels.size());
            for (int p = 1; p <= numberOfTotalChannels.size(); p++) {
                String channelUrl = "html/body/div[2]/div[3]/div[2]/div[3]/div[" + p + "]/h5/a";
                String channelUrlText = Xsoup.compile(channelUrl).evaluate(pageSourceTorrentTVDoc).get();
                String[] parts = channelUrlText.split("=");
                String[] channelSplit = parts[2].split("\"");
                String aceStreamChannelLink = "http://content.asplaylist.net/" + channelSplit[0] + ".acelive";
                String channelNameText = channelSplit[1].replaceAll(" AceStream", "").replaceAll("> <strong>", "").replaceAll(" </strong> </a>", "");
                System.out.println(aceStreamChannelLink);
                System.out.println(channelNameText);
                if (isUTF8MisInterpreted(channelNameText)) {
                    if (checkIfChannelIsInTheList(channels, channelNameText)) {
                        System.out.println("Adding new AceStream link to existing channel " + channelNameText);
                        addingAceLinkToExistingChannel(channels, channelNameText, "Torrent TV",
                                aceStreamChannelLink);
                    } else {
                        addingNewChannel(channelNameText, "Torrent TV", aceStreamChannelLink, channels);
                    }
                }
            }
        } catch (org.openqa.selenium.TimeoutException te) {
            System.out.println("TimeoutException");

        } catch (UnhandledAlertException uae) {
            System.out.println("UnhandledAlertException");
        }
        return channels;
    }

    public ArrayList<ChannelInfo> parseFromLiveFootbalol(WebDriver driver, ArrayList<ChannelInfo> channels) {
        try {
            driver.get("http://www.livefootballol.com/acestream-channel-list.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e) {
            //Do Nothing
        }

        String LiveFootballolAceStreamHtml = driver.getPageSource();

        Document liveFootballolAceStreamDoc = Jsoup.parse(LiveFootballolAceStreamHtml);

        Elements result = Xsoup.compile("//div[@id='system']/article/div/div[2]/table/tbody/tr")
                .evaluate(liveFootballolAceStreamDoc).getElements();

        int size = result.size();

        for (int i = 1; i < size + 1; i++) {
            String channel;
            String aceStreamLink;
            String potentialChannel =
                    "//div[@id='system']/article/div/div[2]/table/tbody/tr[" + Integer.toString(i)
                            + "]/td[2]/strong/a";

            if (Xsoup.select(liveFootballolAceStreamDoc, potentialChannel).get() != null) {
                Document doc =
                        Jsoup.parse(Xsoup.select(liveFootballolAceStreamDoc, potentialChannel).get());
                channel = doc.body().text();
            } else {
                String auxChannel =
                        "//div[@id='system']/article/div/div[2]/table/tbody/tr[" + Integer.toString(i)
                                + "]/td[2]/a/strong";
                Document doc =
                        Jsoup.parse(Xsoup.select(liveFootballolAceStreamDoc, auxChannel).get());
                channel = doc.body().text();
            }
            String aceStreamHtml =
                    "//div[@id='system']/article/div/div[2]/table/tbody/tr[" + Integer.toString(i)
                            + "]/td[3]";
            Document aceStreamLinkDoc =
                    Jsoup.parse(Xsoup.select(liveFootballolAceStreamDoc, aceStreamHtml).get());
            aceStreamLink = aceStreamLinkDoc.body().text();

            String newChannel =
                    channel.replaceAll(" AceStream", "");
            if (checkIfChannelIsInTheList(channels, newChannel)) {
                System.out.println("Adding new AceStream link to existing channel " + newChannel);
                addingAceLinkToExistingChannel(channels, newChannel, "Live Footballol",
                        aceStreamLink);
            } else {
                if (!newChannel.toLowerCase().contains("arenavision")) {
                    addingNewChannel(newChannel, "Live Footballol", aceStreamLink, channels);
                }
            }
        }
        return channels;
    }

    public ArrayList<Events> addingEventsFromSportEvents(WebDriver driver, Date now, ArrayList<Events> listOfEvents, ArrayList<ChannelInfo> channels) {
        //Match between AceStream Available channels and Events
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1;
        String currentMonth;
        if (month <= 10) {
            currentMonth = "0" + Integer.toString(month);
        } else {
            currentMonth = Integer.toString(month);
        }

        int day = cal.get(Calendar.DAY_OF_MONTH);

        //Football
        System.out.println(
                "https://www.sporteventz.com/en/component/magictable/?Itemid=0&se_date=" + Integer
                        .toString(year) + "-" + currentMonth + "-" + Integer.toString(day)
                        + "%2000:00:00&se_module=bW9kX3Nwb3J0ZXZlbnRzX2ZpbHRlcg==&se_id=U2NoZWR1bGU=");

        try {
            driver.get(
                    "https://www.sporteventz.com/en/component/magictable/?Itemid=0&se_date=" + Integer
                            .toString(year) + "-" + currentMonth + "-" + Integer.toString(day)
                            + "%2000:00:00&se_module=bW9kX3Nwb3J0ZXZlbnRzX2ZpbHRlcg==&se_id=U2NoZWR1bGU=");
        } catch (org.openqa.selenium.TimeoutException e) {
            System.out.println("No adding any new channel for Football");
        }


        System.out.println(driver.getCurrentUrl());

        String SportEventzFootballHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventzFootballHtml, listOfEvents, channels, "SOCCER");
        try

        {
            driver.get("https://www.sporteventz.com/en/other-sport/american-football.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e)

        {
            System.out.println("No adding any new channel for American Football");
        }


        String SportEventzAmericanFootballHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventzAmericanFootballHtml, listOfEvents, channels,
                "AMERICAN FOOTBALL");
        try

        {
            driver.get("https://www.sporteventz.com/en/other-sport/basketball.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e)

        {
            System.out.println("No adding any new channel for Basketball");
        }


        String SportEventzBasketBallHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventzBasketBallHtml, listOfEvents, channels, "BASKETBALL");

        try

        {
            driver.get("https://www.sporteventz.com/en/other-sport/handball.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e)

        {
            System.out.println("No adding any new channel for Handball");
        }


        String SportEventzHandBallHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventzHandBallHtml, listOfEvents, channels, "HANDBALL");

        try

        {
            driver.get("https://www.sporteventz.com/en/other-sport/ice-hockey.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e)

        {
            System.out.println("No adding any new channel for Ice Hockey");
        }


        String SportEventzIceHockeyHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventzIceHockeyHtml, listOfEvents, channels, "ICE HOCKEY");
        try

        {
            driver.get("https://www.sporteventz.com/en/other-sport/tennis.html");
            System.out.println(driver.getCurrentUrl());
        } catch (org.openqa.selenium.TimeoutException e)

        {
            System.out.println("No adding any new channel for Tennis");
        }


        String SportEventTennisHtml = driver.getPageSource();

        listOfEvents = sportEventzParsing(SportEventTennisHtml, listOfEvents, channels, "TENNIS");


        getLiveSoccerChannels(driver, listOfEvents, channels);

        return listOfEvents;
    }

    public ArrayList<Events> addingMotorSports(ArrayList<Events> listOfEvents, ArrayList<ChannelInfo> channels) {
        for (Events event : listOfEvents) {
            String motorSport = event.eventName.toLowerCase().replaceAll(" ", "");
            if (motorSport.contains("motogp")) {
                //event.channelsInfo.add(getChannelFromList(channels, "Movistar MotoGP"));
                event.channelsInfo.add(getChannelFromList(channels, "BT Sport 2"));
                event.channelsInfo.add(getChannelFromList(channels, "Viasat Motor"));
            } else if (motorSport.contains("formula1")) {
                //event.channelsInfo.add(getChannelFromList(channels, "Movistar F1"));
                event.channelsInfo.add(getChannelFromList(channels, "Sky Sports F1"));
                event.channelsInfo.add(getChannelFromList(channels, "Viasat Motor"));
            }
        }
        return listOfEvents;
    }

    public ArrayList<Events> addingAgendaFromArenaVision(WebDriver driver, Date now, HashMap<String, List<String>> agendaChannels, ArrayList<ChannelInfo> channels, ArrayList<Events> listOfEvents, SimpleDateFormat dateFormat) throws ParseException, InterruptedException {
        try {
            driver.get("http://arenavision.in/agenda");
            Thread.sleep(15000);
            try
            {
                driver.findElement(By.xpath(".//*[@id='smrt_adfly']/div/span[text()='Close']")).click();
            }
            catch (Exception e) {
            }

            String responseFromAgenda = driver.getPageSource();
            String[] agendaParsing = responseFromAgenda.split("<p>");
            String[] agendaParsing2 = agendaParsing[2].split("</p>");
            String agenda = agendaParsing2[0].replaceAll("<br />", "").replaceAll("\\*", "").replaceAll("<br>", "");

            String[] splittedAgenda = agenda.split("\n");
            for (int i = 0; i < splittedAgenda.length; i++) {
                String[] avs = splittedAgenda[i].split("/AV");
                List<String> avsAces = new ArrayList<>();
                for (int j = 1; j < avs.length; j++) {
                    if (isInteger(avs[j])) {
                        if (Integer.parseInt(avs[j].trim()) < 21) {
                            avsAces.add(avs[j]);
                        }
                    }
                }
                String channeDateDay = avs[0].substring(0, 2);
                String channeDateMonth = avs[0].substring(3, 5);
                String channeDateYear = avs[0].substring(6, 8);
                String channelDate = channeDateYear + "/" + channeDateMonth + "/" + channeDateDay;
                String streamChannelDate = channelDate + " " + avs[0].substring(9, 18);
                Date channelStreamDate = dateFormat.parse(streamChannelDate);
                Date yesterday = DateUtils.addHours(now, -3);

                if (!avs[0].contains("*") && channelStreamDate.compareTo(yesterday) > 0) {
                    agendaChannels
                            .put(channelDate + " " + avs[0].substring(9, avs[0].length()), avsAces);
                }
            }
        } catch (org.openqa.selenium.TimeoutException e) {
            System.out.println("Error getting Arena Vision Agenda");
        } catch (org.openqa.selenium.UnhandledAlertException e) {
            System.out.println("Arena Vision Agenda not available");
        }


        for (Map.Entry<String, List<String>> entry : agendaChannels.entrySet()) {
            String key = entry.getKey().replaceAll("'", "").replaceAll("-", " - ");
            List<String> value = entry.getValue();
            ArrayList<ChannelInfo> channelsInfo = new ArrayList<>();
            for (int i = 0; i < channels.size(); i++) {
                for (String val : value) {
                    if (channels.get(i).channelName.replace("Arena Vision ", "").equals(val)) {
                        channelsInfo.add(new ChannelInfo(channels.get(i).channelName,
                                getLogo(channels.get(i).channelName),
                                getCategory(channels.get(i).channelName)));
                    }
                }
            }
            listOfEvents.add(new Events(key, channelsInfo));
        }
        return listOfEvents;
    }

    public ArrayList<ChannelInfo> parseFromArenaVision(WebDriver driver, ArrayList<ChannelInfo> channels) throws InterruptedException {
        //Parsing for ArenaVision AceStreamLinks
        ArenaVisionPage arenaVisionPage = new ArenaVisionPage(driver);
        for (int i = 1; i < 21; i++) {
            try {
                driver.get("http://arenavision.in/av" + i);
                System.out.println(driver.getCurrentUrl());
                if (driver.getCurrentUrl().contains("arenavision")) {
                    String response = driver.getPageSource();
                    Thread.sleep(1000);
                    String aceStreamLink = arenaVisionPage.getAceStreamLinkFromResponse(response);
                    addingNewChannel("Arena Vision " + i, "Arena Vision", aceStreamLink,
                            channels);
                } else {
                    throw new TimeoutException();
                }
            } catch (TimeoutException e) {
                System.out.println("Arena Vision " + Integer.toString(i) + " not available");
            } catch (UnhandledAlertException e) {
                System.out.println("Arena Vision " + Integer.toString(i) + " not available");
            }
        }
        return channels;
    }
}
