package com.acenhauer.parser.helpers;

import java.util.ArrayList;

/**
 * Created by guillem on 15/01/16.
 */
public class Events {

    public String eventName;
    public ArrayList<ChannelInfo> channelsInfo;

    public Events(String eventName, ArrayList<ChannelInfo> channelsInfo) {
        this.eventName = eventName;
        this.channelsInfo = channelsInfo;
    }

}

