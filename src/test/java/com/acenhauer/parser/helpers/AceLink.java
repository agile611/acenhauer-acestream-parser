package com.acenhauer.parser.helpers;

/**
 * Created by guillem on 15/01/16.
 */

public class AceLink {

    public String acelink;
    public String source;

    public AceLink(String source, String acelink) {
        this.acelink = acelink;
        this.source = source;
    }
}
